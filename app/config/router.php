<?php

$router = $di->getRouter();

// Home / route:
$router->add(
    "/",
    [
        "controller" => "index",
        "action" => "index"
    ]
)->setName("home");

// Querys SQL
$router->add(
    "/index/query",
    [
        "controller" => "index",
        "action" => "query"
    ]
)->setName("querys");

// Users
$router->add(
    "/users",
    [
        "controller" => "users",
        "action" => "index"
    ]
)->setName("users");

$router->handle($_SERVER['REQUEST_URI']);
