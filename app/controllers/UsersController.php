<?php
declare(strict_types=1);



use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;


class UsersController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        // Renderiza la vista para la página de usuarios
        $this->view->pick('users/index');

        $paginator   = new Model(
            [
                'model'      => Users::class,
                'limit'      => 5,
                'page' => $this->request->getQuery('page', 'int', 1),
            ]
        );

        $this->view->page = $paginator->paginate();
        $this->view->setVar('user', new Users());
    }

    /**
     * Searches for users
     */
    public function searchAction()
    {

        $first_name = $this->request->get('first_name');
        $last_name = $this->request->get('last_name');
        $email = $this->request->get('email');

        if ( $first_name || $last_name || $email ) {
            $numberPage = $this->request->getQuery('page', 'int', 1);

            //! Fixbug: Elimino el nodo 'search' del array
            // Error: PHP Notice:  Undefined index: search in phalcon/Mvc/Model/Criteria.zep on line 331
            //    Array ( [first_name] => 434 [last_name] => [email] => [search] => Buscar )
            $fixGetData = $this->request->get();
            unset($fixGetData['search']);

            $parameters = Criteria::fromInput($this->di, 'Users', $fixGetData)->getParams();
            $parameters['order'] = "id";

            $paginator   = new Model(
                [
                    'model'      => 'Users',
                    'parameters' => $parameters,
                    'limit'      => 10,
                    'page'       => $numberPage,
                ]
            );

            $paginate = $paginator->paginate();
            if (0 === $paginate->getTotalItems()) {

                $this->flashSession->notice("No se encontraron registros.");

                $this->dispatcher->forward([
                    "controller" => "users",
                    "action" => "index"
                ]);
            }

            $this->view->setVars([
                "page"      => $paginate,
                "firstName" => $first_name ? $first_name : '__',
                "lastName" => $last_name ? $last_name : '__',
                "email"     => $email ? $email : '__',
            ]);

        } else {
            $this->flashSession->warning("Nada por buscar, por favor diligencie algún campo para realizar búsqueda...");
            return $this->response->redirect('/users');
        }

        $this->view->msg = $first_name;

    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->setVar('user', new Users());
    }

    /**
     * Edits a user
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $user = Users::findFirstByid($id);
            if (!$user) {
                $this->userNotFoundMessage();

                $this->dispatcher->forward([
                    'controller' => "users",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $user->getId();
            $this->view->setVar('user', $user);

        }
    }

    /**
     * Creates a new user
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        try {

            $user = new Users();
            $user->setfirstName($this->request->getPost("first_name"));
            $user->setlastName($this->request->getPost("last_name"));
            $user->setemail($this->request->getPost("email", "email"));


            if (!$user->save()) {
                foreach ($user->getMessages() as $message) {
                    $this->flashSession->error($message->getMessage());
                }

                $this->dispatcher->forward([
                    'controller' => "users",
                    'action' => 'new'
                ]);

                return;
            }

            $this->flashSession->success("Nuevo usuario creado correctamente.");

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

        } catch (\Exception $e) {

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            $this->flashSession->error($e);
        }
    }

    /**
     * Saves a user edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $user = Users::findFirstByid($id);

        if (!$user) {
            $this->flashSession->error("El usuario no existe " . $id);

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        $user->setfirstName($this->request->getPost("first_name"));
        $user->setlastName($this->request->getPost("last_name"));
        $user->setemail($this->request->getPost("email", "email"));

        if (!$user->save()) {

            foreach ($user->getMessages() as $message) {
                $this->flashSession->error($message->getMessage());
            }

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'edit',
                'params' => [$user->getId()]
            ]);

            return;
        }

        $this->flashSession->success("El usuario ($id) fue actualizado correctamente.");

        $this->dispatcher->forward([
            'controller' => "users",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a user
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $user = Users::findFirstByid($id);
        if (!$user) {
            $this->userNotFoundMessage();

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        if (!$user->delete()) {

            foreach ($user->getMessages() as $message) {
                $this->flashSession->error($message->getMessage());
            }

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'search'
            ]);

            return;
        }

        $this->flashSession->success("Usuario eliminado...");

        $this->dispatcher->forward([
            'controller' => "users",
            'action' => "index"
        ]);
    }

    private function userNotFoundMessage()
    {
        return $this->flashSession->error("El usuario no fue encontrado");
    }
}
