<?php
declare(strict_types=1);

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    /**
     * Carga de archivos personalizados css y js
     * @return void
     */
    public function beforeExecuteRoute() {
        $this->assets
            ->addCss('css/app.css');

        $this->assets
            ->addJs('js/app.js');
    }
}
