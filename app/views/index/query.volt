
<div class="row py-4">
    <div class="col text-center">
        <h4>Query para crear la BD</h4>
    </div>
</div>

<div class="row">

    <div class="col">
        <code>
            <pre>
              CREATE TABLE users (
                id INT AUTO_INCREMENT PRIMARY KEY,
                first_name VARCHAR(50) NOT NULL,
                last_name VARCHAR(50) NULL,
                email VARCHAR(100) NOT NULL
              );
            </pre>
        </code>

    </div>
</div>

<div class="row py-4">
    <div class="col text-center">
        <h4>Query para poblar la BD</h4>
    </div>
</div>

<div class="row">

    <div class="col">
        <code>
            <pre>
                INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`) VALUES
                (1, 'User 1', 'App 1', 'test1@test.com'),
                (2, 'User 2', 'App 2', 'test2@test.com'),
                (3, 'User 3', 'App 3', 'test3@test.com'),
                (4, 'User 4', 'App 4', 'test4@test.com'),
                (5, 'User 5', 'App 5', 'test6@test.com'),
                (6, 'User 6', 'App 6', 'test6@test.com'),
                (7, 'User 7', 'App 7', 'test7@test.com'),
                (8, 'User 8', 'App 8', 'test8@test.com'),
                (9, 'User 9', 'App 9', 'test9@test.com'),
                (10, 'User 10', 'App 10', 'test10@test.com'),
                (11, 'User 11', 'App 11', 'test11@test.com');
            </pre>
        </code>

    </div>
</div>
