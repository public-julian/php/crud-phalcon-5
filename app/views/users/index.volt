<h1 class="mt-4">Listado de usuarios</h1>

<div class="row">
    <div class="col py-4">
        <a class="btn btn-outline-primary" href="/users/new">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4"/>
            </svg>&nbsp;&nbsp;Crear
        </a>
    </div>
</div>

<div class="row">
    <div class="col">
        <form action="{{ url('users/search') }}" class="form-horizontal" method="get">

            <div class="row">

                <div class="col">
                    <div class="form-group">
                        <label for="fieldFirstName" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            {{ inputText("first_name", user.first_name, ["size" : 30, "class" : "form-control", "id" : "fieldFirstName"]) }}
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label for="fieldLastName" class="col-sm-2 control-label">Apellido</label>
                        <div class="col-sm-10">
                            {{ inputText("last_name", user.last_name, ["size" : 30, "class" : "form-control", "id" : "fieldLastName"]) }}
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label for="fieldEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            {{ inputText("email", user.email, ["size" : 30, "class" : "form-control", "id" : "fieldEmail"]) }}
                        </div>
                    </div>
                </div>

                <div class="col d-inline-flex pt-4 pb-3">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ inputSubmit('search', 'Buscar', ['class': 'btn btn-dark']) }}
                    </div>
                </div>
            </div>
        </form>

        {{ flashSession.output() }}
    </div>
</div>

<div class="row">
    <div class="col table-responsive">
        <table class="table table-hover">
            <thead class="table-dark">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>

                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {% for user in page.getItems() %}
                <tr>
                    <td>{{ user.id }}</td>
                    <td>{{ user.first_name }}</td>
                    <td>{{ user.last_name }}</td>
                    <td>{{ user.email }}</td>

                    <td class="text-end">
                        <a class="btn btn-outline-secondary" href="{{ "/users/edit/"~user.id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
                                <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001m-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708z"/>
                            </svg>&nbsp;Editar
                        </a>
                    </td>
                    <td>
                        <a class="btn btn-outline-danger" href="{{ "/users/delete/"~user.id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/>
                                <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/>
                            </svg> Borrar
                        </a>
                    </td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-sm-1">
        <p class="pagination" style="line-height: 1.42857;padding: 6px 12px;">
            {{ page.getCurrent()~"/"~page.getTotalItems() }}
        </p>
    </div>
    <div class="col-sm-11">
        <nav>
            <ul class="pagination">
                <li>
                    {{ a(url("users/"), "Primero", ["class": "page-link", 'id': 'first']) }}
                </li>
                <li>
                    <a class="page-link" href="{{ "/users?page="~page.getPrevious() }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-skip-start" viewBox="0 0 16 16">
                            <path d="M4 4a.5.5 0 0 1 1 0v3.248l6.267-3.636c.52-.302 1.233.043 1.233.696v7.384c0 .653-.713.998-1.233.696L5 8.752V12a.5.5 0 0 1-1 0zm7.5.633L5.696 8l5.804 3.367z"/>
                        </svg>
                    </a>
                </li>
                <li>

                    <a class="page-link" href="{{ "/users?page="~page.getNext() }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-skip-end" viewBox="0 0 16 16">
                            <path d="M12.5 4a.5.5 0 0 0-1 0v3.248L5.233 3.612C4.713 3.31 4 3.655 4 4.308v7.384c0 .653.713.998 1.233.696L11.5 8.752V12a.5.5 0 0 0 1 0zM5 4.633 10.804 8 5 11.367z"/>
                        </svg>
                    </a>
                </li>
                <li>{{ a(url("users?page="~page.getLast()), "Último", ["class": "page-link", 'id': 'last']) }}</li>
            </ul>
        </nav>
    </div>
</div>
