
<div class="row">
    <div class="col py-4">
        <a class="icon-link icon-link-hover link-success link-underline-success link-underline-opacity-25" href="/users/">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply-fill" viewBox="0 0 16 16">
                <path d="M5.921 11.9 1.353 8.62a.72.72 0 0 1 0-1.238L5.921 4.1A.716.716 0 0 1 7 4.719V6c1.5 0 6 0 7 8-2.5-4.5-7-4-7-4v1.281c0 .56-.606.898-1.079.62z"/>
            </svg>
            Volver
        </a>
    </div>
</div>

<div class="text-center t-4">
    <h1>Nuevo registro</h1>
</div>

{{ flashSession.output() }}

<div class="container">
    <form action="{{ url('users/create') }}" class="pt-5" method="post">

        <div class="mb-3">
            <label for="fieldFirstName" class="form-label">Nombre</label>
            {{ inputText("first_name", user.first_name, ["size" : 30, "class" : "form-control", "id" : "fieldFirstName", "required" : "required"]) }}
        </div>

        <div class="mb-3">
            <label for="fieldLastName" class="form-label">Apellido</label>
            {{ inputText("last_name", user.last_name, ["size" : 30, "class" : "form-control", "id" : "fieldLastName"]) }}
        </div>

        <div class="mb-3">
            <label for="fieldEmail" class="form-label">Email</label>
            {{ inputText("email", user.email, ["size" : 30, "class" : "form-control", "id" : "fieldEmail", "required" : "required"]) }}
        </div>

        <div class="form-group d-flex text-center pt-4">
            <div class="col-sm-offset-2 col-sm-10">
                {{ inputSubmit('save', 'Guardar', ['class': 'btn btn-primary btn-lg']) }}
            </div>
        </div>
    </form>
</div>
